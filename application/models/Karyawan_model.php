<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "master_karyawan";
	
	public function tampilDataMasterKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataMasterKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM master_karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataMasterKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik']			= $this->input->post('nik');
		$data['nama']			= $this->input->post('nama');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tanggal_lahir']		= $tgl_gabung;
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
		
	}
	
	
	public function delete($nik)
	{
		//delete from db
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama']			= $this->input->post('nama');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tanggal_lahir']	= $tgl_gabung;
		$data['flag']			= 1;
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
}
