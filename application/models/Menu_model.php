<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model
{
	//panggil nama table
	private $_table = "master_menu";
	
	public function tampilDataMasterMenu()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataMasterMenu2()
	{
		$query = $this->db->query("SELECT * FROM master_menu WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataMasterMenu3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_menu', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_menu)
	{
		$this->db->select('*');
		$this->db->where('kode_menu', $kode_menu);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		
		$data['kode_menu']		= $this->input->post('kode_menu');
		$data['nama_menu']		= $this->input->post('nama_menu');
		$data['harga']			= $this->input->post('harga');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
		
	}
	
	public function delete($kode_menu)
	{
		//delete from db
		$this->db->where('kode_menu', $kode_menu);
		$this->db->delete($this->_table);
	}
	
	public function update($kode_menu)
	{
		$data['nama_menu']		= $this->input->post('nama_menu');
		$data['harga']			= $this->input->post('harga');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']			= 1;
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update($this->_table, $data);
	}
	
	public function cariHargaMenu($kode_menu)
	{
		$query	= $this->db->query("SELECT * FROM " . $this->_table . " 
		WHERE flag = 1 AND kode_menu = '$kode_menu'");
        $hasil = $query->result();

		foreach ($hasil as $data){
			$harganya = $data->harga;
		}
		
		return $harganya;
	}

}
