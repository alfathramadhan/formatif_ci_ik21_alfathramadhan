<!DOCTYPE html>
<html>
<head>
	<title>Toko Pizza</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>index.php/Master_karyawan/listmasterkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>index.php/Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="#">Pemesanan</a></li>
        </ul>
    </li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>EDIT DATA KARYAWAN</b><br>
                </div>
            </div>
<?php
	foreach ($detail_menu as $data) {
		$kode_menu		= $data->kode_menu;
		$nama_menu			= $data->nama_menu;
		$harga			= $data->harga;
		$keterangan		= $data->keterangan;
	}
?>
    <form action="<?=base_url()?>index.php/master_menu/edit/<?=$kode_menu;?>" method="post">

<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Menu</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_menu" id="kode_menu" value="<?=$kode_menu;?>" maxlength="20" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_menu" id="nama_menu" value="<?=$nama_menu;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Harga</td>
    <td>:</td>
    <td><input type="text" name="harga" id="harga" value="<?=$harga?>" /></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><input type="text" name="keterangan" id="keterangan" value="<?=$keterangan?>" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
  </form>
</table>
</div>
</body>
</html>