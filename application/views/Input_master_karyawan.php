<!DOCTYPE html>
<html>
<head>
	<title>Toko Pizza</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>index.php/Master_karyawan/listmasterkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>index.php/Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="#">Pemesanan</a></li>
        </ul>
    </li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>INPUT DATA KARYAWAN</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>index.php/master_karyawan/input" method="post">
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" maxlength="20">
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama" id="nama" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" /></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" /></td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl=1;$tgl<=31;$tgl++){
	 ?>
     	<option value="<?=$tgl;?>"><?=$tgl;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=0;$bln<12;$bln++){
	  ?>
      <option value="<?=$bln+1;?>">
      		<?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn <= date('Y')-15;$thn++){
	  ?>
      	<option value="<?=$thn;?>"><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>index.php/master_karyawan/listmasterkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>