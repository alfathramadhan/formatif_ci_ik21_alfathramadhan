<!DOCTYPE html>
<html>
<head>
	<title>Toko Pizza</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>index.php/Master_karyawan/listmasterkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>index.php/Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>index.php/Trans_pemesanan/listtranspemesanan">Pemesanan</a></li>
        </ul>
    </li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DATA KARYAWAN</b><br>
                </div>
    
    <ul>
    &nbsp;
    </ul>
    
    <table width="100%" border="0">
      <tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>NIK</td>
        <td>Nama</td>
        <td>Alamat</td>
        <td>Tempat Lahir</td>
        <td>Tanggal Lahir</td>
        <td>Telp</td>
        <td>Aksi</td>
        </td>
      </tr>
<?php
	$no = 0;
	foreach ($data_master_karyawan as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->nik; ?></td>
        <td><?= $data->nama; ?></td>
        <td><?= $data->alamat; ?></td>
        <td><?= $data->tempat_lahir; ?></td>
        <td><?= $data->tanggal_lahir; ?></td>
        <td><?= $data->telp; ?></td>
        <td>
        <a href="<?=base_url(); ?>index.php/master_karyawan/input">Input</a>
        | <a href="<?=base_url(); ?>index.php/master_karyawan/edit/<?= $data->nik; ?>">Edit</a>
        | <a href="<?=base_url(); ?>index.php/master_karyawan/delete/<?= $data->nik; ?>
        "onclick="return confirm('Yakin Ingin hapus Data?');">Delete</a>
        </td>
      </tr>
<?php } ?>
    </table>
    		</div>
       </div>
</body>
</html>