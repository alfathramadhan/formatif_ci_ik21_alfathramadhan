<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
	}
	
	public function index()
	{
		$this->listmasterkaryawan();
	}
	
	public function listmasterkaryawan()
	{
		$data['data_master_karyawan'] = $this->karyawan_model->tampilDataMasterKaryawan();
		$this->load->view('home_karyawan', $data);
	}
	
	public function input()
	{
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("index.php/master_karyawan/listmasterkaryawan", "refresh");
		}
		$this->load->view('input_master_karyawan');
	}
	
	public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("/index.php/master_karyawan/listmasterkaryawan", "refresh");
	}
	public function edit($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->update($nik);
			redirect("index.php/master_karyawan/listmasterkaryawan", "refresh");
		}
		$this->load->view('edit_master_karyawan', $data);
	}
}